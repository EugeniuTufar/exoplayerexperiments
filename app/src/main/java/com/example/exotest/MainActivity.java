package com.example.exotest;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.exotest.player.PlayerActivity;
import com.example.exotest.player2.PlayerActivity2;

public class MainActivity extends AppCompatActivity {
    private Button startVideo;
    private Button startVideo2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        startVideo = (Button) findViewById(R.id.videoStart);
        startVideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, PlayerActivity.class);
                startActivity(intent);
            }
        });

        startVideo2 = (Button) findViewById(R.id.videoStart2);
        startVideo2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, PlayerActivity2.class);
                startActivity(intent);
            }
        });
    }
}
