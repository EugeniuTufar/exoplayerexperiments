package com.example.exotest.player2.sync;

import android.util.Log;

import com.example.exotest.player2.sync.intervals.IntervalListener;
import com.google.android.exoplayer2.Format;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.audio.AudioRendererEventListener;
import com.google.android.exoplayer2.decoder.DecoderCounters;

/**
 * Created by eugeniu on 3/9/17.
 */

public class AudioMonitor implements AudioRendererEventListener {

    private final String TAG = getClass().getSimpleName();
    private final long MAX_TIME_DELAY = 5023646;

    private SimpleExoPlayer audioPlayer;
    private IntervalListener intervalListener;

    private long interval;
    private boolean initialized;

    public AudioMonitor(SimpleExoPlayer audioPlayer, IntervalListener intervalListener) {
        this.audioPlayer = audioPlayer;
        this.intervalListener = intervalListener;
        audioPlayer.setAudioDebugListener(this);
    }

    @Override
    public void onAudioEnabled(DecoderCounters counters) {
        synchronized (audioPlayer) {
            Log.e(TAG, "onAudioEnabled: ");
            initialized = true;
            audioPlayer.setPlayWhenReady(true);
        }
    }

    @Override
    public void onAudioSessionId(int audioSessionId) {
        Log.d(TAG, "onAudioSessionId: ");

    }

    @Override
    public void onAudioDecoderInitialized(String decoderName, long initializedTimestampMs, long initializationDurationMs) {
        Log.d(TAG, "onAudioDecoderInitialized: timestamp: " /*+ initializedTimestampMs + " duration " + initializationDurationMs*/);
    }

    @Override
    public synchronized void onAudioInputFormatChanged(Format format) {
        Log.d(TAG, "onAudioInputFormatChanged: interval: " + (System.nanoTime() - interval) + "<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<");
//        if (System.nanoTime() - interval < 20047766132L) {
//            try {
//                wait(System.nanoTime() - interval);
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
//            Log.e(TAG, "onAudioInputFormatChanged: STOPED");
//        }
        intervalListener.onSourceFinished(System.currentTimeMillis(), 0);
        interval = System.nanoTime();
    }

    @Override
    public void onAudioTrackUnderrun(int bufferSize, long bufferSizeMs, long elapsedSinceLastFeedMs) {
        Log.d(TAG, "onAudioTrackUnderrun: ");
    }

    @Override
    public void onAudioDisabled(DecoderCounters counters) {
        Log.d(TAG, "onAudioDisabled: ");
    }

}
