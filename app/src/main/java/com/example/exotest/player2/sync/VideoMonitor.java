package com.example.exotest.player2.sync;

import android.util.Log;
import android.view.Surface;

import com.example.exotest.player2.sync.intervals.IntervalListener;
import com.google.android.exoplayer2.Format;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.decoder.DecoderCounters;
import com.google.android.exoplayer2.video.VideoRendererEventListener;

/**
 * Created by eugeniu on 3/9/17.
 */

public class VideoMonitor implements VideoRendererEventListener {
    private final String TAG = getClass().getSimpleName();
    private final long MAX_TIME_DELAY = 5023646;

    private SimpleExoPlayer videoPlayer;
    private IntervalListener intervalListener;

    private long interval;
    private int cycleCount;

    public VideoMonitor(SimpleExoPlayer videoPlayer, IntervalListener intervalListener) {
        this.videoPlayer = videoPlayer;
        this.intervalListener = intervalListener;
        videoPlayer.setVideoDebugListener(this);
    }

    int count = 0;

    @Override
    public void onVideoEnabled(DecoderCounters counters) {
        Log.e(TAG, "onVideoEnabled: ");
//        synchronized (locker) {
//            videoPlayer.setPlayWhenReady(true);
//        }
    }

    @Override
    public void onVideoDecoderInitialized(String decoderName, long initializedTimestampMs, long initializationDurationMs) {
        Log.e(TAG, "onVideoDecoderInitialized: timestamp: " + initializedTimestampMs + " duration " + initializationDurationMs);

    }

    @Override
    public void onVideoInputFormatChanged(Format format) {
        Log.e(TAG, "onVideoInputFormatChanged: " + ++count + ", interval: " + (System.nanoTime() - interval));
        interval = System.nanoTime();
        intervalListener.onSourceFinished(System.currentTimeMillis(), ++cycleCount);
    }

    @Override
    public void onDroppedFrames(int count, long elapsedMs) {
        Log.e(TAG, "onDroppedFrames: ");
    }

    @Override
    public void onVideoSizeChanged(int width, int height, int unappliedRotationDegrees, float pixelWidthHeightRatio) {
        Log.e(TAG, "onVideoSizeChanged: ");
    }

    @Override
    public void onRenderedFirstFrame(Surface surface) {
        Log.e(TAG, "onRenderedFirstFrame: ");
        videoPlayer.setPlayWhenReady(true);
    }

    @Override
    public void onVideoDisabled(DecoderCounters counters) {
        Log.e(TAG, "onVideoDisabled: ");
    }

}
