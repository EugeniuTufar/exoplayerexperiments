package com.example.exotest.player2.sync;

import android.util.Log;

import com.example.exotest.player2.sync.intervals.IntervalListener;
import com.google.android.exoplayer2.SimpleExoPlayer;

/**
 * Created by eugeniu on 3/10/17.
 */

public class SyncPlayers {
    private final long MINIMAL_TIME_UNIT_VALUE = 2002;
    private final long MAX_TIME_DELAY = 10;

    private SimpleExoPlayer playerAudio;
    private SimpleExoPlayer playerVideo;

    private long videoTimestamp;
    private long audioTimestamp;

    public SyncPlayers(SimpleExoPlayer playerAudio, SimpleExoPlayer playerVideo) {
        this.playerAudio = playerAudio;
        this.playerVideo = playerVideo;
        runMonitors();
    }

    private void runMonitors() {
        final AudioMonitor audioMonitor = new AudioMonitor(playerAudio, new IntervalListener() {
            @Override
            public void onSourceFinished(long timestamp, int cycleCount) {
                audioTimestamp = timestamp;
            }
        });
        final VideoMonitor videoMonitor = new VideoMonitor(playerVideo, new IntervalListener() {
            @Override
            public void onSourceFinished(long timestamp, int cycleCount) {
                videoTimestamp = timestamp;
                if (cycleCount % 2 == 0) {
                    syncMonitors();
                }
            }
        });
    }
    private void syncMonitors() {
//        Log.e("SYNC", "syncMonitors: " + (audioTimestamp - videoTimestamp));
        Log.e("SYNC", "syncMonitors: " + (playerVideo.getCurrentPosition() - playerAudio.getCurrentPosition()));
        Log.e("SYNC", "syncMonitors: " + calculateMaxDelay());
        long interval = Math.abs(playerAudio.getCurrentPosition() - playerVideo.getCurrentPosition());
//        if (interval > calculateMaxDelay()) {
        long positionMs = playerVideo.getCurrentPosition() - interval;
        Log.e("SYNC", "syncMonitors: sync threads: positionMS: " + positionMs);
        Log.e("SYNC", "syncMonitors: sync threads: curPos: " + playerVideo.getCurrentPosition());
        playerAudio.seekTo(playerVideo.getCurrentPosition() * getDurationConstant());
//        }
    }

    private long calculateMaxDelay() {
        return Math.abs(playerAudio.getDuration() - playerVideo.getDuration()) + MAX_TIME_DELAY;
    }

    private long getDurationConstant() {
        if (playerVideo.getDuration() / playerAudio.getDuration() > 1) {
            return playerAudio.getDuration() / playerVideo.getDuration();
        }
        return playerVideo.getDuration() / playerAudio.getDuration();
    }
}
