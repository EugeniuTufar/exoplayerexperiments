package com.example.exotest.player2.sync.intervals;

/**
 * Created by eugeniu on 3/10/17.
 */

public interface IntervalListener {
    void onSourceFinished(long timestamp, int cycleCount);
}
