package com.example.exotest.player2;

import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;

import com.example.exotest.R;
import com.example.exotest.player2.sync.SyncPlayers;
import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.DefaultLoadControl;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.LoadControl;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory;
import com.google.android.exoplayer2.extractor.ExtractorsFactory;
import com.google.android.exoplayer2.source.ClippingMediaSource;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.LoopingMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.trackselection.AdaptiveVideoTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.ui.SimpleExoPlayerView;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;

/**
 * Created by eugeniu on 3/6/17.
 */

public class PlayerActivity2 extends AppCompatActivity {
    private final String TAG = getClass().getSimpleName();

    private SimpleExoPlayer playerVideo;
    private SimpleExoPlayer playerAudio;

    private SimpleExoPlayerView videoSimpleExoPlayer;
    private SimpleExoPlayerView audioSimpleExoPlayer;
    private Button triggerEventButton;

    public static final String WORKOUT_ID_KEY = "workout_key";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_player);

        videoSimpleExoPlayer = (SimpleExoPlayerView) findViewById(R.id.playerVideoView);
        audioSimpleExoPlayer = (SimpleExoPlayerView) findViewById(R.id.playerAudioView);

        triggerEventButton = (Button) findViewById(R.id.triggerCache);

        initPlayer();
    }

    private void initPlayer() {
        Handler mainHandler = new Handler();
        DefaultBandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
        TrackSelection.Factory videoTrackSelectionFactory =
                new AdaptiveVideoTrackSelection.Factory(bandwidthMeter);
        TrackSelector trackSelector =
                new DefaultTrackSelector(videoTrackSelectionFactory);

        LoadControl loadControl = new DefaultLoadControl();

        playerVideo = ExoPlayerFactory.newSimpleInstance(this, trackSelector, loadControl);
        playerAudio = ExoPlayerFactory.newSimpleInstance(this, trackSelector, loadControl);
        // Produces DataSource instances through which media data is loaded.
        DataSource.Factory dataSourceFactory = new DefaultDataSourceFactory(this,
                Util.getUserAgent(this, getPackageName()), null);
        ExtractorsFactory extractorsFactory = new DefaultExtractorsFactory();

        Uri[] uris = getBlockMediaSource2();

        MediaSource videoSource = new ExtractorMediaSource(uris[0],
                dataSourceFactory, extractorsFactory, mainHandler, null);
        MediaSource oneTwoSource = new ExtractorMediaSource(uris[1],
                dataSourceFactory, extractorsFactory, mainHandler, null);

        ClippingMediaSource clippedVideoMS = new ClippingMediaSource(videoSource, 0, (long)(4.004 * C.MICROS_PER_SECOND));
        ClippingMediaSource clippedAudioMS = new ClippingMediaSource(oneTwoSource, 0, (long)(2.002 * C.MICROS_PER_SECOND));

        LoopingMediaSource loopingVideoSource = new LoopingMediaSource(clippedVideoMS);
        LoopingMediaSource loopingAudioSource = new LoopingMediaSource(clippedAudioMS);

        playerVideo.prepare(loopingVideoSource);
        playerAudio.prepare(loopingAudioSource);

        videoSimpleExoPlayer.setPlayer(playerVideo);
        audioSimpleExoPlayer.setPlayer(playerAudio);

        SyncPlayers syncPlayers = new SyncPlayers(playerAudio, playerVideo);
//        mainHandler.post(videoThread);
    }

    public Uri[] getBlockMediaSource2() {
        return new Uri[]{Uri.parse("file:///android_asset/videos/exercise4s.mp4"),
//                Uri.parse("file:///android_asset/additional_audio/one_two_en.mp3"),
                Uri.parse("file:///android_asset/additional_audio/one_two_en.mp3")};
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.item1:
                Log.e(TAG, "onOptionsItemSelected: positionV " + playerVideo.getBufferedPosition() + " percent: " + playerVideo.getBufferedPercentage());
                Log.e(TAG, "onOptionsItemSelected: positionA " + playerAudio.getBufferedPosition() + " percent: " + playerAudio.getBufferedPercentage());
                playerAudio.notify();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        playerVideo.release();
        playerAudio.release();
    }
}
