package com.example.exotest.player;

import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Surface;
import android.widget.Button;

import com.example.exotest.R;
import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.DefaultLoadControl;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.Format;
import com.google.android.exoplayer2.LoadControl;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.audio.AudioRendererEventListener;
import com.google.android.exoplayer2.decoder.DecoderCounters;
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory;
import com.google.android.exoplayer2.extractor.ExtractorsFactory;
import com.google.android.exoplayer2.source.ClippingMediaSource;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.LoopingMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.MergingMediaSource;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.AdaptiveVideoTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.ui.SimpleExoPlayerView;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;
import com.google.android.exoplayer2.video.VideoRendererEventListener;

import java.util.List;

/**
 * Created by eugeniu on 3/6/17.
 */

public class PlayerActivity extends AppCompatActivity {
    private final String TAG = getClass().getSimpleName();

    private SimpleExoPlayerView videoSimpleExoPlayer;
    private SimpleExoPlayerView audioSimpleExoPlayer;
    private Button triggerEventButton;

    private SimpleExoPlayer playerVideo;

    public static final String WORKOUT_ID_KEY = "workout_key";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_player);

        videoSimpleExoPlayer = (SimpleExoPlayerView) findViewById(R.id.playerVideoView);
        audioSimpleExoPlayer = (SimpleExoPlayerView) findViewById(R.id.playerAudioView);

        triggerEventButton = (Button) findViewById(R.id.triggerCache);


        initPlayer();
//        initObservablePLayer();
//        initMultiplePLayers();
    }



//    private void initMultiplePLayers() {
//        MultiplePlayer multiplePlayer = new MultiplePlayer(this, getBlockMediaSource());
//        SimpleExoPlayer simpleExoPlayer = multiplePlayer.initPlayers();
//        simpleExoPlayer.setPlayWhenReady(true);
//        videoSimpleExoPlayer.setPlayer(simpleExoPlayer);
//    }

    private void initPlayer() {
        Handler mainHandler = new Handler();
        DefaultBandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
        TrackSelection.Factory videoTrackSelectionFactory =
                new AdaptiveVideoTrackSelection.Factory(bandwidthMeter);
        TrackSelector trackSelector =
                new DefaultTrackSelector(videoTrackSelectionFactory);

        LoadControl loadControl = new DefaultLoadControl();

        playerVideo = ExoPlayerFactory.newSimpleInstance(this, trackSelector, loadControl);
//        SimpleExoPlayer playerAudio = ExoPlayerFactory.newSimpleInstance(this, trackSelector, loadControl);
        // Produces DataSource instances through which media data is loaded.
        DataSource.Factory dataSourceFactory = new DefaultDataSourceFactory(this,
                Util.getUserAgent(this, getPackageName()), null);
        ExtractorsFactory extractorsFactory = new DefaultExtractorsFactory();

        Uri[] uris = getBlockMediaSource2();

        MediaSource videoSource = new ExtractorMediaSource(uris[0],
                dataSourceFactory, extractorsFactory, mainHandler, null);
        MediaSource oneTwoSource = new ExtractorMediaSource(uris[1],
                dataSourceFactory, extractorsFactory, mainHandler, null);

        final ClippingMediaSource clippedVideoMS = new ClippingMediaSource(videoSource, 0, (long)(2.002 * C.MICROS_PER_SECOND));
        final ClippingMediaSource clippedAudioMS = new ClippingMediaSource(oneTwoSource, 0, (long)(2.002 * C.MICROS_PER_SECOND));

        final MergingMediaSource mergedMediaSource = new MergingMediaSource(clippedVideoMS, clippedAudioMS);

        final LoopingMediaSource loopingVideoSource = new LoopingMediaSource(mergedMediaSource);


        // Prepare the player with the source.
        playerVideo.prepare(loopingVideoSource);
        playerVideo.setVideoDebugListener(getVideoDebugListener(clippedVideoMS, clippedAudioMS));
        playerVideo.setAudioDebugListener(getAudioDebugListener());
//        playerVideo.addListener(new ExoPlayer.EventListener() {
//            private boolean discontinuedTrack;
//
//            @Override
//            public void onTimelineChanged(Timeline timeline, Object manifest) {
//                Log.e(TAG, "onTimelineChanged: TimeLine periodCount: " + timeline.getPeriodCount() + " TimeLine windows count: " +
//                        timeline.getWindowCount() + " TimeLine isEmpty " + timeline.isEmpty());
//            }
//
//            @Override
//            public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {
//                Log.e(TAG, "onTracksChanged: TrackGroups: " + trackGroups.length + " TrackSelections: " + trackSelections.length);
//            }
//
//            @Override
//            public void onLoadingChanged(boolean isLoading) {
//                Log.e(TAG, "onLoadingChanged: is loading " + isLoading);
//            }
//
//            @Override
//            public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
//                Log.e(TAG, "onPlayerStateChanged: playWhenReady " + playWhenReady + " playBackState " + playbackState);
//                Log.e(TAG, "onPlayerStateChanged: position " + playerVideo.getBufferedPosition()
//                        + " percent: " + playerVideo.getBufferedPercentage()
//                        + " duration " + playerVideo.getDuration());
//
//
//                if (discontinuedTrack && playerVideo.getDuration() != C.TIME_UNSET &&
//                        playerVideo.getBufferedPosition() != playerVideo.getDuration()) {
//                    Log.e(TAG, "onVideoInputFormatChanged: PLAYER STUCK, REINITIALIZE...");
//                    discontinuedTrack = false;
//                    playerVideo.prepare(new LoopingMediaSource(new MergingMediaSource(clippedVideoMS, clippedAudioMS)), false, false);
//                }
////                switch (playbackState) {
////                    case 1:
////                        playerVideo.setPlayWhenReady(true);
////                        break;
////                    case 4:
////                        playerVideo.prepare(mergedMediaSource, false, false);
////                        playerVideo.setPlayWhenReady(false);
////                }
////                if (discontinuedTrack && playbackState == 4) {
////                    discontinuedTrack = false;
////                    playerVideo.prepare(mergedMediaSource, false, false);
////                }
//            }
//
//            @Override
//            public void onPlayerError(ExoPlaybackException error) {
//                Log.e(TAG, "onPlayerError: ");
//            }
//
//            @Override
//            public void onPositionDiscontinuity() {
//                Log.e(TAG, "onPositionDiscontinuity: ");
//                discontinuedTrack = true;
//            }
//        });
        playerVideo.setPlayWhenReady(true);
        videoSimpleExoPlayer.setPlayer(playerVideo);

    }

    long videoLocker;
    long audioLocker;
    private VideoRendererEventListener getVideoDebugListener(final MediaSource mediaSource1, final MediaSource mediaSource2) {
        return new VideoRendererEventListener() {
            int count = 0;
            @Override
            public void onVideoEnabled(DecoderCounters counters) {
                Log.e(TAG, "onVideoEnabled: ");
            }

            @Override
            public void onVideoDecoderInitialized(String decoderName, long initializedTimestampMs, long initializationDurationMs) {
                Log.e(TAG, "onVideoDecoderInitialized: ");

            }

            @Override
            public void onVideoInputFormatChanged(Format format) {
//                Log.e(TAG, "onVideoInputFormatChanged: " + ++count + " position "
//                        + playerVideo.getBufferedPosition() + " percent: " + playerVideo.getBufferedPercentage()
//                        + " duration " + playerVideo.getDuration());
                Log.e(TAG, "onVideoInputFormatChanged: " + ++count + " difference " + (audioLocker - videoLocker));
                videoLocker = System.nanoTime();

//                if (playerVideo.getDuration() != C.TIME_UNSET &&
//                        playerVideo.getBufferedPosition() != playerVideo.getDuration()) {
//                    Log.e(TAG, "onVideoInputFormatChanged: PLAYER STUCK, REINITIALIZE...");
//                    playerVideo.prepare(new LoopingMediaSource(new MergingMediaSource(mediaSource1, mediaSource2)), false, false);
//                }
            }

            @Override
            public void onDroppedFrames(int count, long elapsedMs) {
                Log.e(TAG, "onDroppedFrames: ");
            }

            @Override
            public void onVideoSizeChanged(int width, int height, int unappliedRotationDegrees, float pixelWidthHeightRatio) {
                Log.e(TAG, "onVideoSizeChanged: ");
            }

            @Override
            public void onRenderedFirstFrame(Surface surface) {
                Log.e(TAG, "onRenderedFirstFrame: ");
            }

            @Override
            public void onVideoDisabled(DecoderCounters counters) {
                Log.e(TAG, "onVideoDisabled: ");
            }
        };
    }

    private AudioRendererEventListener getAudioDebugListener() {
        return new AudioRendererEventListener() {
            @Override
            public void onAudioEnabled(DecoderCounters counters) {
                Log.d(TAG, "onAudioEnabled: ");
            }

            @Override
            public void onAudioSessionId(int audioSessionId) {
                Log.d(TAG, "onAudioSessionId: ");
            }

            @Override
            public void onAudioDecoderInitialized(String decoderName, long initializedTimestampMs, long initializationDurationMs) {
                Log.d(TAG, "onAudioDecoderInitialized: ");
            }

            @Override
            public void onAudioInputFormatChanged(Format format) {
                Log.d(TAG, "onAudioInputFormatChanged: ");
                audioLocker = System.nanoTime();
            }

            @Override
            public void onAudioTrackUnderrun(int bufferSize, long bufferSizeMs, long elapsedSinceLastFeedMs) {
                Log.d(TAG, "onAudioTrackUnderrun: ");
            }

            @Override
            public void onAudioDisabled(DecoderCounters counters) {
                Log.d(TAG, "onAudioDisabled: ");
            }
        };
    }

    public Uri[] getBlockMediaSource2() {
        return new Uri[]{Uri.parse("file:///android_asset/videos/exercise.mp4"),
//                Uri.parse("file:///android_asset/additional_audio/one_two_en.mp3"),
                Uri.parse("file:///android_asset/additional_audio/one_two_en.mp3")};
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.item1:
                Log.e(TAG, "onOptionsItemSelected: positionL " + playerVideo.getBufferedPosition() + " percent: " + playerVideo.getBufferedPercentage());
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

        @Override
    public void onBackPressed() {
        super.onBackPressed();
        playerVideo.release();
    }
}
